from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in programming_app/__init__.py
from programming_app import __version__ as version

setup(
	name="programming_app",
	version=version,
	description="cleint side scripting",
	author="poonam",
	author_email="xyz",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
